<?php

/**
 * @file
 * Extends the MediaJoveStreamWrapper class to make it suitable for local testing.
 */

/**
 *  Create an instance like this:
 *  $wrapper = new MediaJoveTestStreamWrapper('jove://v/[video-code]');
 */
class MediaJoveTestStreamWrapper extends MediaJoveStreamWrapper {

}
