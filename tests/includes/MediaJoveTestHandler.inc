<?php

/**
 * @file
 * Extends the MediaInternetJoveHandler class to make it suitable for local testing.
 */

/**
 * A test handler for JoVE videos.
 *
 * @see MediaInternetJoveHandler().
 */
class MediaJoveTestHandler extends MediaInternetJoveHandler {

}
