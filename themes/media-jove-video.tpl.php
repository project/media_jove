<?php

/**
 * @file media_jove/themes/media-jove-video.tpl.php
 *
 * Template file for theme('media_jove_video').
 *
 * Variables available:
 *  $uri - The media uri for the JoVE video (e.g., jove://v/59216).
 *  $video_id - The unique identifier of the JoVE video (e.g., 59216).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the JoVE iframe.
 *  $options - An array containing the Media JoVE formatter options.
 *  $width - The width value set in Media: JoVE file display options.
 *  $height - The height value set in Media: JoVE file display options.
 *  $title - The Media: JoVE file's title.
 */

?>
<div class="<?php print $classes; ?> media-jove-file-<?php print $id; ?> media-jove-video-<?php print $video_id; ?>">
  <iframe class="media-jove-player"
          allowTransparency="true"
          allowfullscreen
          height="<?php print $height; ?>"
          width="<?php print $width; ?>"
          border="0"
          scrolling="no"
          frameborder="0"
          marginwheight="0"
          marginwidth="0"
          src="<?php print $url; ?>">
  </iframe>
</div>
