<?php

/**
 * @file media_jove/themes/media_jove.theme.inc
 *
 * Theme and preprocess functions for Media: JoVE.
 */

/**
 * Preprocess function for theme('media_jove_video').
 *
 * @param $variables
 */
function media_jove_preprocess_media_jove_video(&$variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  if ($wrapper instanceof MediaReadOnlyStreamWrapper) {
    $parts = $wrapper->get_parameters();
    $variables['embed_type'] = 'video';
    $variables['video_id'] = check_plain($parts['v']);
    $variables['options']['id'] = check_plain($parts['v']);
    if (isset($parts['a'])) {
      $variables['options']['access'] = check_plain($parts['a']);
    }
    $embed_path = '/embed/player';
  }
  else {
    // This happens when stream wrappers are not yet initialized. This is
    // normally only encountered when creating content during profile install
    // using drush make. At that point, video_id is irrelevant anyway.
    $variables['video_id'] = '';
    return;
  }

  // Checked existing function.
  if(function_exists('file_uri_to_object')) {
    // Make the file object available.
    $file_object = file_uri_to_object($variables['uri'], TRUE);
  }
  else {
    $file_object = media_jove_file_uri_to_object($variables['uri']);
  }

  // Parse options and build the query string. Only add the option to the query
  // array if the option value is not default.
  $query = array();

  $query_variables = [
    'title' => 't',
    'description' => 'i',
    'authors' => 'a',
    'chapters' => 'chap',
    'autoplay' => 's',
  ];

  // Video ID.
  $query['id'] = $variables['options']['id'];
  if (!empty($variables['options']['access'])) {
    $query['access'] = $variables['options']['access'];
  }

  // These queries default to 0. If the option is true, set value to 1.
  foreach (array('title', 'description', 'authors', 'chapters', 'autoplay') as $option) {
    if (isset($variables['options'][$option]) && $variables['options'][$option]) {
      $query[$query_variables[$option]] = 1;
    }
  }

  // Add some options as their own template variables.
  foreach (array('width', 'height') as $theme_var) {
    $variables[$theme_var] = $variables['options'][$theme_var];
  }

  // Handle width and height depending on video options.
  if ($variables['options']['title']) {
    $variables['height'] += 50;
  }
  if ($variables['options']['authors']) {
    $variables['height'] += 100;
  }
  if ($variables['options']['description']) {
    $variables['height'] += 150;
  }
  if ($variables['options']['chapters']) {
    $variables['width'] += 280;
  }

  // Do something useful with the overridden attributes from the file
  // object. We ignore alt and style for now.
  if (isset($variables['options']['attributes']['class'])) {
    if (is_array($variables['options']['attributes']['class'])) {
      $variables['classes_array'] = array_merge($variables['classes_array'], $variables['options']['attributes']['class']);
    }
    else {
      // Provide nominal support for Media 1.x.
      $variables['classes_array'][] = $variables['options']['attributes']['class'];
    }
  }

  // Add template variables for accessibility.
  $variables['title'] = check_plain($file_object->filename);

  // Build the iframe URL with options query string.
  $variables['url'] = url('https://www.jove.com' . $embed_path, array('query' => $query, 'external' => TRUE));
}

/**
 * Helper function.
 * It will work for the case then 'file_uri_to_object' function is not accessible.
 *
 * @param $uri
 *
 * @return bool|mixed|\stdClass
 */
function media_jove_file_uri_to_object($uri) {
  $uri = file_stream_wrapper_uri_normalize($uri);
  $files = entity_load('file', FALSE, array('uri' => $uri));
  $file = !empty($files) ? reset($files) : FALSE;
  if (!$file) {
    global $user;
    $file = new stdClass();
    $file->uid = $user->uid;
    $file->filename = basename($uri);
    $file->uri = $uri;
    $file->filemime = file_get_mimetype($uri);
    // This is gagged because some uris will not support it.
    $file->filesize = @filesize($uri);
    $file->timestamp = REQUEST_TIME;
    $file->status = FILE_STATUS_PERMANENT;
  }
  return $file;
}
