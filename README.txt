CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage

INTRODUCTION
------------

JoVE creates the ultimate solutions for advancing research and science education by making and publishing videos of scientific experiments from the top laboratories around the globe.

By allowing scientists, educators and students to see the intricate details of cutting-edge experiments rather than read them in text articles, JoVE increases STEM research productivity and student learning, saving their institutions time and money.

That’s why today more than 1,000 of the world’s top universities, colleges, hospitals and biotech companies depend on JoVE to maximize productivity, efficiency and successful outcomes.

JoVE creates scientific videos for two unique resources: JoVE Video Journal and JoVE Science Education Library.

Media: JoVE adds JoVE as a supported media provider.

REQUIREMENTS
------------

Media: JoVE has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: JoVE can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: JoVE integrates the JoVE video-sharing service with the Media
module to allow users to add and manage JoVE videos as they would any other
piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: JoVE enabled, users can add a JoVE video by entering its URL
or embed code.
