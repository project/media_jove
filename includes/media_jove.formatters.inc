<?php

/**
 * @file
 * File formatters for JoVE videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_jove_file_formatter_info() {
  $formatters['media_jove_video'] = array(
    'label' => t('JoVE Video'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => 460,
      'height' => 365,
      'title' => FALSE,
      'description' => FALSE,
      'authors' => FALSE,
      'chapters' => FALSE,
      'autoplay' => FALSE,
    ),
    'view callback' => 'media_jove_file_formatter_video_view',
    'settings callback' => 'media_jove_file_formatter_video_settings',
    'mime types' => array('video/jove'),
  );

  $formatters['media_jove_image'] = array(
    'label' => t('JoVE Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_jove_file_formatter_image_view',
    'settings callback' => 'media_jove_file_formatter_image_settings',
    'mime types' => array('video/jove'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 *
 * @param $file
 * @param $display
 * @param $langcode
 *
 * @return array
 */
function media_jove_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'jove' && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'media_jove_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    foreach (array('width', 'height', 'title', 'description', 'authors', 'chapters', 'autoplay', 'attributes') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    // Invert 'autoplay' option regarding the JoVE embed settings.
    $element['#options']['autoplay'] = $element['#options']['autoplay'] == 1 ? 0 : 1;

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_jove_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  // Video width.
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
    '#element_validate' => array('_media_jove_validate_video_width_and_height'),
  );

  // Video height.
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
    '#element_validate' => array('_media_jove_validate_video_width_and_height'),
  );

  // Title option.
  $element['title'] = array(
    '#title' => t('Display title'),
    '#type' => 'checkbox',
    '#default_value' => $settings['title'],
  );

  // Description option.
  $element['description'] = array(
    '#title' => t('Display description'),
    '#type' => 'checkbox',
    '#default_value' => $settings['description'],
  );

  // Authors option.
  $element['authors'] = array(
    '#title' => t('Display authors'),
    '#type' => 'checkbox',
    '#default_value' => $settings['authors'],
  );

  // Chapters option.
  $element['chapters'] = array(
    '#title' => t('Display chapters'),
    '#type' => 'checkbox',
    '#default_value' => $settings['chapters'],
  );

  // Autoplay option.
  $element['autoplay'] = array(
    '#title' => t('Autoplay video on load'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
  );

  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_jove_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'jove') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);

    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => str_replace('http:', '', $wrapper->getLocalThumbnailPath()),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
        '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
      );
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 *
 * @param $form
 * @param $form_state
 * @param $settings
 *
 * @return array
 */
function media_jove_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();

  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );

  return $element;
}

/**
 * Validation for width and height.
 *
 * @param $element
 * @param $form_state
 * @param $form
 */
function _media_jove_validate_video_width_and_height($element, &$form_state, $form) {
  // Check if the value is a number with an optional decimal or percentage sign, or "auto".
  if (!empty($element['#value']) && !preg_match('/^(auto|([0-9]*(\.[0-9]+)?%?))$/', $element['#value'])) {
    form_error($element, t("The value entered for @dimension is invalid. Please insert a unitless integer for pixels, a percent, or \"auto\". Note that percent and auto may not function correctly depending on the browser and doctype.", array('@dimension' => $element['#title'])));
  }
}
