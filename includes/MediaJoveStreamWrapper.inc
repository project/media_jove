<?php

/**
 *  @file
 *  Extends the MediaReadOnlyStreamWrapper class to handle JoVE videos.
 */

/**
 *  Create an instance like this:
 *  $jove = new MediaJoveStreamWrapper('jove://v/[video-code]');
 */
class MediaJoveStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'https://www.jove.com/embed/player';

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/jove';
  }

  function getOriginalThumbnailPath() {
    return 'https://www.jove.com/img/embed_poster.jpg';
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $id = $parts['v'];
    $local_path = file_default_scheme() . '://media-jove/' . check_plain($id) . '.jpg';
    if (!file_exists($local_path)) {
      // getOriginalThumbnailPath throws an exception if there are any errors
      // when retrieving the original thumbnail from JoVE.
      try {
        $dirname = drupal_dirname($local_path);
        file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
        $response = drupal_http_request($this->getOriginalThumbnailPath());

        if (!isset($response->error)) {
          file_unmanaged_save_data($response->data, $local_path, TRUE);
        }
        else {
          system_retrieve_file($this->getOriginalThumbnailPath(), $local_path, FALSE, FILE_EXISTS_REPLACE);
        }
      }
      catch (Exception $e) {
        // In the event of an endpoint error, use the mime type icon provided
        // by the Media module.
        $file = file_uri_to_object($this->uri);
        $icon_dir = variable_get('media_icon_base_directory', 'public://media-icons') . '/' . variable_get('media_icon_set', 'default');
        $local_path = file_icon_path($file, $icon_dir);
      }
    }

    return $local_path;
  }

  /**
   * Returns a url in the format "https://www.jove.com/embed/player?id=59216".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   */
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {

      if (isset($parameters['v'])) {
        $parameters['id'] = $parameters['v'];
        unset($parameters['v']);
      }
      if (isset($parameters['a'])) {
        $parameters['access'] = $parameters['a'];
        unset($parameters['a']);
      }

      return $this->base_url . '?' . http_build_query($parameters);
    }
  }

}
