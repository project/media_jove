<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle JoVE videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetJoveHandler extends MediaInternetBaseHandler {

  public function parse($embedCode) {

    // A special case when user can enter the video ID.
    if ((int) $embedCode != 0 && ((int) $embedCode) == $embedCode) {
      $uri = 'jove://v/' . $embedCode;
      $this->video_id = $embedCode;
      return file_stream_wrapper_uri_normalize($uri);
    }

    // https://www.jove.com/v/62589/optical-tweezers-to-study-rna-protein-interactions-translation
    // https://www.jove.com/video/59216/cortisol-measurement-in-koala-phascolarctos-cinereus-fur
    // https://www.jove.com/science-education/5201/an-introduction-to-neurophysiology
    // https://www.jove.com/embed/directions/59216
    // https://www.jove.com/embed/player?id=50306&t=1&a=1&i=1&chap=1&s=1

    $patterns = array(
      '@jove\.com/v/([0-9]+)/([^"#\&\? ]+)@i',
      '@jove\.com/video/([0-9]+)/([^"#\&\? ]+)@i',
      '@jove\.com/science-education/([0-9]+)/([^"#\&\? ]+)@i',
      '@jove\.com/embed/directions/([0-9]+)@i',
      '@jove\.com/embed/player[#\?].*?id=([0-9]+)(&access=([0-9A-z]+))?@i',
    );

    foreach ($patterns as $pattern) {
      preg_match_all($pattern, $embedCode, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1][0]) && $this->validId($matches[1][0])) {
        $this->video_id = $matches[1][0];
        $uri = 'jove://v/' . $matches[1][0];
        if (isset($matches[3]) && $matches[3][0]) {
          $this->access = $matches[3][0];
          $uri .= '/a/' . $this->access;
        }
        return file_stream_wrapper_uri_normalize($uri);
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    // Try to default the file name to the video's title.
    if ((empty($file->fid) || empty($file->filename)) && $info = $this->getInfo()) {
      $file->filename = truncate_utf8($info['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media.
   *
   * @param $video_id
   *
   * @return mixed|void Information about the media.
   *   Information about the media.
   * @throws \Exception
   */
  public function getInfo() {
    if ($cache = cache_get("jove_info:$this->video_id")) {
      $info = $cache->data;
      return $info;
    }
    else {
      $info = array();
      $title = '';
      $uri = $this->parse($this->embedCode);
      $info_url = file_create_url($uri);
      $video_id = $this->video_id;
      if (empty($video_id)) {
        preg_match("/(https:\/\/www.jove.com\/embed\/player?).*(id=)(\d+)/", $info_url, $matches);
        if ($matches && $matches[1] === 'https://www.jove.com/embed/player' && $matches[3]) {
          $video_id = $matches[3];
        }
      }
      if ($video_id) {
        // Try API.
        $api_info_url = 'https://app.jove.com/api/free/article/en/' . $video_id;

        $response = drupal_http_request($api_info_url);
        if (!isset($response->error)) {
          $joveApiData = json_decode($response->data, TRUE);

          // Note that below data structure might change, as it seems to be
          // an internal API.
          if (!empty($joveApiData['content']['domain']['title'])) {
            // Strip HTML tags.
            $title = strip_tags($joveApiData['content']['domain']['title']);
            // Make sure it fits in the field.
            $title = mb_substr(trim($title), 0, 250);
          }
        }
        else {
          throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
        }
      }

      if ($title === '') {
        // Still no title found for video.
        $title = 'Jove video id: ' . $this->video_id;
      }

      $info['title'] = trim($title);

      cache_set("jove_info:$this->video_id", $info);
      return $info;
    }
  }

  /**
   * Check if a JoVE video ID is valid.
   *
   * @param $id
   *
   * @return boolean
   *   TRUE if the video ID is valid, or throws a
   *   MediaInternetValidationException otherwise.
   * @throws \MediaInternetValidationException
   */
  public function validId($id) {
    if ($cache = cache_get("jove:$id")) {
      $valid = $cache->data;
    }
    else {
      $embed_url = url('https://www.jove.com/embed/directions/'.$id);
      $response = drupal_http_request($embed_url, array('method' => 'GET'));
      if ($response->code == 404 || $response->code != 200) {
        $valid = FALSE;
      }
      else {
        $valid = TRUE;
      }

      cache_set("jove:$id", $valid);
    }

    if (!$valid) {
      throw new MediaInternetValidationException(t('This JoVE video doesn\'t exist or the service is not accessible.'));
    }

    return TRUE;
  }
}
