<?php

/**
 * @file
 * File hooks implemented by the Media: JoVE module.
 */

/**
 * Implements hook_file_operations().
 */
function media_jove_file_operations() {
  $operations = array(
    'media_jove_refresh' => array(
      'label' => t('Refresh JoVE information from source'),
      'callback' => 'media_jove_cache_clear',
    ),
  );

  return $operations;
}

/**
 * Clear the cached JoVE content for the selected files.
 */
function media_jove_cache_clear($fids) {
  $fids = array_keys($fids);

  $query = new EntityFieldQuery();
  $results = $query
    ->entityCondition('entity_type', 'file')
    ->propertyCondition('uri', 'jove:', 'STARTS_WITH')
    ->propertyCondition('fid', $fids)
    ->execute();

  $files = file_load_multiple(array_keys($results['file']));

  foreach ($files as $file) {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $local_path = $wrapper->getLocalThumbnailPath();
    file_unmanaged_delete($local_path);
    // Fix missing file name.
    if (empty($file->filename)) {
      $video_id = $wrapper->get_parameters()['v'];
      if ($video_id) {
        // Try API.
        $api_info_url = 'https://app.jove.com/api/free/article/en/' . $video_id;

        $response = drupal_http_request($api_info_url);
        if (!isset($response->error)) {
          $joveApiData = json_decode($response->data, TRUE);

          // Note that below data structure might change, as it seems to be
          // an internal API.
          if (!empty($joveApiData['content']['domain']['title'])) {
            $title = strip_tags($joveApiData['content']['domain']['title']);
            $file->filename = mb_substr($title, 0, 250);
            file_save($file);
          }
        }
      }
    }
  }
}
